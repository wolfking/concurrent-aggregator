# concurrent-aggregator

#### 介绍
并发聚合器，一些无关的调用，并行处理，最后添加聚合器
#### 使用说明

1. 启动类添加注解
###
    @EnableConcurrentAggregate("com.digibig.service.im.aggregator")
2. 写代理的接口
###
    @ConcurrentAggregate
    public interface DemoAggregateProxy {
        @AggregateProviders({
                @AggregateProvider(beanClass = StudentService.class, method = "getStudentById", parameters = @AggregateParameter(name = "ids")),
                @AggregateProvider(beanClass = TeacherService.class, method = "getTeacherByStudentId", parameters = @AggregateParameter(name = "ids"))})
        @AggregateConsumer(beanClass = StudentTeacherAggregator.class,method = "studentWithTeachers")
        List<Student> demoStudent(@AggregateParameterName("ids") List<Integer> ids);
    }
3. 注入接口使用
###
        @Autowired
        @SuppressWarnings("all")
        private DemoAggregateProxy demoAggregateProxy;



