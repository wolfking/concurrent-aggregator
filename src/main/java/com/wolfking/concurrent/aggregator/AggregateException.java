package com.wolfking.concurrent.aggregator;

/**
 * <p>
 *
 * @author 赵伟伟(wolfking)
 * created on 2019-07-10 10:46
 * Email is zhaoweiwei@youxin.com
 * Copyright is 友信金服
 */
public class AggregateException extends RuntimeException {

    public AggregateException(String message) {
        super(message);
    }
}
