package com.wolfking.concurrent.aggregator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * 并发方法的配置
 *
 * @author 赵伟伟(wolfking)
 * created on 2019-07-10 10:34
 * Email is zhaoweiwei@youxin.com
 * Copyright is 友信金服
 */

@AllArgsConstructor
class AggregateMethodConfig {

    @Getter
    private List<AggregateProviderConfig> aggregateProviderConfigList;

    @Getter
    private AggregateConsumerConfig aggregateConsumerConfig;

    @Getter
    private long timeout;
}
