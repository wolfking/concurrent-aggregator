package com.wolfking.concurrent.aggregator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.util.Arrays;
import java.util.Set;

/**
 * Import的并发聚合的scanner 自动注册spring的bean
 * 经过隔壁老王处理的JDK动态代理的并发聚合处理器
 *
 * @author 赵伟伟(wolfking)
 * Created on 2019/7/15 14:48
 * Email is zhaoweiwei@ehomepay.com.cn
 * Copyright is 理房通
 */
@Slf4j
class ConcurrentAggregateScanner extends ClassPathBeanDefinitionScanner {

    ConcurrentAggregateScanner(BeanDefinitionRegistry registry) {
        super(registry, false);
    }

    /**
     * Calls the parent search that will search and register all the candidates.
     * Then the registered objects are post processed to set them as
     * SpiSelectorFactoryBeans
     */
    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);
        if (beanDefinitions.isEmpty()) {
            log.warn("no current aggregate interface was found in {} package. please check your configuration.", Arrays.toString(basePackages));
        } else {
            log.info("find total {} current aggregate interface", beanDefinitions.size());
            beanDefinitions.forEach(this::assemblyBeanDefinition); /*注册bean*/
        }
        return beanDefinitions;
    }

    /**
     * 构建spring注册bean的信息
     * 设置 @Primary属性
     * 使用工厂类创建
     *
     * @param holder spring注册bean的holder
     */
    private void assemblyBeanDefinition(BeanDefinitionHolder holder) {
        /*scan的bean都是GenericBeanDefinition类型*/
        GenericBeanDefinition definition = GenericBeanDefinition.class.cast(holder.getBeanDefinition());
        String spiInterface = definition.getBeanClassName();
        log.info("register the concurrent aggregator interface is {}", spiInterface);
        /*设置工厂类的class*/
        definition.setBeanClass(ConcurrentAggregateProxyFactory.class);
        /*设置构造参数的class*/
        definition.getConstructorArgumentValues().addGenericArgumentValue(spiInterface);
        /*设置自动注入的类型，根据Type*/
        definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
        /*设置Primary属性，相当于@Primary注解*/
        definition.setPrimary(true);
        log.info("register the concurrent aggregate interface {} success", spiInterface);
    }

    /**
     * 注册扫描的过滤器
     */
    void registerFilters() {
        /*添加SpiInterface的的注解过滤器*/
        addIncludeFilter(new AnnotationTypeFilter(ConcurrentAggregate.class));
        /*添加package-info的过滤器*/
        addExcludeFilter((reader, factory) -> reader.getClassMetadata().getClassName().endsWith("package-info"));
    }


    /**
     * 是否是spring的注册bean
     *
     * @param beanDefinition bean的定义
     * @return 是否是托管给spring管理的Bean
     */
    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        return beanDefinition.getMetadata().isInterface() && beanDefinition.getMetadata().isIndependent();
    }
}
