package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;

/**
 * 并发聚合的动态代理的接口
 * 经过隔壁老王处理的JDK动态代理的接口
 *
 * @author 赵伟伟(wolfking)
 * Created on 2019/7/15 14:48
 * Email is zhaoweiwei@ehomepay.com.cn
 * Copyright is 理房通
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConcurrentAggregate {
}
