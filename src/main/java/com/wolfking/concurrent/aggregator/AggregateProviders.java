package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateProviders {
    AggregateProvider[] value();

    /**
     * 超时，时间是毫秒
     *
     * @return
     */
    long timeout() default 10000;
    /**
     * 不考虑并发中的异常
     *
     * @return
     */
    boolean ignoreException() default false;
	
	
}
