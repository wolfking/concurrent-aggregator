package com.wolfking.concurrent.aggregator;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 并发聚合的启动类的注解
 *
 * @author 赵伟伟(wolfking)
 * Created on 2019/7/15 14:48
 * Email is zhaoweiwei@ehomepay.com.cn
 * Copyright is 理房通
 */
@Documented
@Target(ElementType.TYPE)
@Import(ConcurrentAggregateRegistrar.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableConcurrentAggregate {
    String[] value() default "com.wolfking";
}
