package com.wolfking.concurrent.aggregator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.Method;

/**
 * <p>
 *
 * @author 赵伟伟(wolfking)
 * created on 2019-07-10 10:35
 * Email is zhaoweiwei@youxin.com
 * Copyright is 友信金服
 */
@AllArgsConstructor
class AggregateConsumerConfig {
    /**
     * 实体
     */
    @Getter
    private Object bean;

    /**
     * 方法
     */
    @Getter
    private Method method;

    /**
     * 舍弃放回结果
     */
    @Getter
    private boolean discardResult;
}
