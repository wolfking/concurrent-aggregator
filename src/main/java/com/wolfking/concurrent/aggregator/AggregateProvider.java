package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateProvider {

    /**
     * spring的bean的名字
     *
     * @return
     */
    String beanName() default "";

    /**
     * spring托管bean的类型
     *
     * @return
     */
    Class<?> beanClass() default Void.class;

    /**
     * 执行的方法名
     *
     * @return
     */
    String method() default "";

    /**
     * 忽略返回值
     *
     * @return
     */
    boolean discardResult() default false;

    /**
     * 参数的集合
     *
     * @return
     */
    AggregateParameter[] parameters() default {};

    /**
     * 并发的维度
     *
     * @return
     */
    int level() default 0;
	
	    /**
     * 忽略异常
     *
     * @return
     */
    boolean ignoreException() default false;
}
