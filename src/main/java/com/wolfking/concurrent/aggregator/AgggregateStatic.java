package com.wolfking.concurrent.aggregator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 *
 * @author 赵伟伟(wolfking)
 * created on 2019-07-10 14:08
 * Email is zhaoweiwei@youxin.com
 * Copyright is 友信金服
 */
public class AgggregateStatic {
    public static ExecutorService executorService = Executors.newFixedThreadPool(1000);
}
