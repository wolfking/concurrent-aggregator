package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;


@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateParameter {

    /**
     * 对应的参数名
     *
     * @return
     */
    String value();
}
