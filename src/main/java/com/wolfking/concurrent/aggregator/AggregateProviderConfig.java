package com.wolfking.concurrent.aggregator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Method;

/**
 * <p>
 *
 * @author 赵伟伟(wolfking)
 * created on 2019-07-10 10:35
 * Email is zhaoweiwei@youxin.com
 * Copyright is 友信金服
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class AggregateProviderConfig {
    /**
     * 实体
     */
    private Object bean;

    /**
     * 方法
     */
    private Method method;
    /**
     * 并发维度
     */
    private int level = 0;

    /**
     * 参数的名字
     */
    private String[] parameterNames = {};

    /**
     * 忽略返回参数
     */
    private boolean discardResult = false;

    /**
     * 超时的毫秒
     */
    private long timeout;
	
	/**
     * 忽略异常
     */
    private boolean ignoreException;
}
