package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateConsumer {

    /**
     * spring的bean的名字
     *
     * @return
     */
    String beanName() default "";

    /**
     * spring托管bean的类型
     *
     * @return
     */
    Class<?> beanClass() default Void.class;

    /**
     * 执行的方法名
     *
     * @return
     */
    String method() default "";

    /**
     * 忽略返回值
     * @return
     */
    boolean discardResult() default false;

}
