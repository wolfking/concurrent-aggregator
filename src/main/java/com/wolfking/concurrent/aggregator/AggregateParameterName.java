package com.wolfking.concurrent.aggregator;

import java.lang.annotation.*;

@Documented
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateParameterName {
    /**
     * 参数名的注解
     *
     * @return
     */
    String value();
}
